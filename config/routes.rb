# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users
  # root "articles#index"
  namespace :api do
    namespace :v1 do
      resources :pricings, only: %i[create simple_quote] do
        collection do
          get :simple_quote
        end
      end
    end
  end
end
