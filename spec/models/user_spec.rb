# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  let!(:user) { create(:user) }
  let(:basic_params) do
    {
      name: Faker::Name.name,
      email: Faker::Internet.email,
      password: Faker::Internet.password
    }
  end

  describe 'create' do
    context 'success' do
      it 'with basic params' do
        user = described_class.new(basic_params)
        expect(user.save).to be_truthy
      end
    end

    context 'failed' do
      it 'wihout email' do
        user = described_class.new(basic_params.merge(email: nil))
        expect(user.save).to be_falsey
      end

      it 'wihout password' do
        user = described_class.new(basic_params.merge(password: nil))
        expect(user.save).to be_falsey
      end
    end
  end

  describe 'update' do
    context 'success' do
      it 'update name' do
        user.update(name: basic_params[:name])
        user.reload
        expect(user.name).to eq basic_params[:name]
      end

      it 'update email' do
        user.update(email: basic_params[:email])
        user.reload
        expect(user.email).to eq basic_params[:email]
      end

      it 'update password' do
        expect(user.update(email: basic_params[:email])).to be_truthy
      end
    end
  end

  describe 'destroy' do
    context 'success' do
      it 'remove user' do
        id = user.id
        user.destroy
        expect(described_class.find_by(id: id)).to be_nil
      end
    end
  end
end
