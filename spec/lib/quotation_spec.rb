# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Quotation do
  let(:amount) { 1_000_000 }
  let(:expiration_date) { '2020-11-30' }

  it 'calculate amount' do
    obj = Quotation.new(document_amount: amount, expiration_date: expiration_date)
    expect(obj.calculate_cost).to eq 19_437
  end

  it 'calculate cost' do
    obj = Quotation.new(document_amount: amount, expiration_date: expiration_date)
    expect(obj.calculate_amount).to eq 946_573
  end

  it 'calculate excess' do
    obj = Quotation.new(document_amount: amount, expiration_date: expiration_date)
    expect(obj.calculate_excess).to eq 10_000
  end
end
