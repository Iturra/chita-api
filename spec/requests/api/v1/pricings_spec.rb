require 'rails_helper'

RSpec.describe 'Api::V1::Pricings', type: :request do
  let(:params) {
    {
      client_dni: '76329692-K',
      debtor_dni: '77360390-1',
      document_amount: 1_000_000,
      folio: 75,
      expiration_date: '2022-11-30'
    }
  }
  it 'GET /simple_quote' do
    get simple_quote_api_v1_pricings_path, params: params
    data = JSON.parse response.body
    expect(data['financing_cost']).to_not be_nil
    expect(data['amount_to_receive']).to_not be_nil
    expect(data['excess']).to_not be_nil
  end
end
