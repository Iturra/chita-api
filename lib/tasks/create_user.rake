# frozen_string_literal: true

desc 'Create test user'
task create_user: [:environment] do
  user = ::User.create(name: 'Prueba Prueba', email: 'prueba@prueba.cl', password: 'prueba123')
  if user.persisted?
    puts 'Se ha creado correctamente'
  else
    puts 'Falló la creacion'
  end
end
