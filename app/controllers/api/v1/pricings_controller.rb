# frozen_string_literal: true

module Api
  module V1
    class PricingsController < ApplicationController
      def simple_quote
        payload = Quotation.new(document_amount: permitted_params[:document_amount],
                                expiration_date: permitted_params[:expiration_date]).build_payload
        render json: payload.to_json, status: 200
      end

      private

      def permitted_params
        params.permit(%i[client_dni debtor_dni document_amount folio expiration_date])
      end
    end
  end
end
