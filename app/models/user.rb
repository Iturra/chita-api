# frozen_string_literal: true

class User < ApplicationRecord
  devise :database_authenticatable, :rememberable, :validatable

  validates :email, presence: true, uniqueness: true
  validates :password, presence: true
end
