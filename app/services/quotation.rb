# frozen_string_literal: true

class Quotation
  DOCUMENT_RATE = 1.9
  COMMISSION = 23_990.0
  ADVANCE_PERCENT = 99.0
  DAYS_RATE = 30

  def initialize(params)
    @document_amount = params[:document_amount].to_f
    @expiration_date = params[:expiration_date]
  end

  def calculate_cost
    @document_amount * (ADVANCE_PERCENT/100) * (((DOCUMENT_RATE/100) / DAYS_RATE) * time_limit)
  end

  def calculate_amount
    (@document_amount * (ADVANCE_PERCENT/100)) - calculate_cost - COMMISSION
  end

  def calculate_excess
    @document_amount - (@document_amount * (ADVANCE_PERCENT/100))
  end

  def build_payload
    {
      financing_cost: calculate_cost.round(0),
      amount_to_receive: calculate_amount.round(0),
      excess: calculate_excess.round(0)
    }
  end

  private

  def time_limit
    (Date.parse(@expiration_date) - Date.today).to_i + 1
  end
end
