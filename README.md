# README

Este proyecto pertenece a la prueba tecnica de Chita

Requisitos:
* ruby 3.0.3

Descarga:
* git clone https://gitlab.com/Iturra/chita-api.git

Instalacion:
* cd chita-api
* bundle install
* rails db:create
* rails db:migrate
* rails create_user (este comando crea un usuario prueba)
* rails s

Ejecutar test:
* bundle exec rspec

Ejecutar request
```
curl --location --request GET 'localhost:3000/api/v1/pricings/simple_quote?client_dni=76329692-K&debtor_dni=77360390-1&document_amount=1000000&folio=75&expiration_date=2022-11-30' \
--header 'Authorization: Basic cHJ1ZWJhQHBydWViYS5jbDpwcnVlYmExMjM='
```


Explicación:
Se hizo una API con autenticacion basada en Basic Auth usando la gema Devise
El unico endpoint funcional es
`GET /api/v1/pricings/simple_quote`

Este endpoint cae en el controlador `app/controllers/api/v1/pricings_controller.rb`
El cual consulta a la libreria **Quotation** para hacer los calculos correspondientes
